#!/bin/sh -e

# Sample Usage:
# $ V=1 TARGET=i386-linux-uclibc ./build_toolchain.sh

# verbose output?
test -n "$V" || V=0
# fallback commands
test -n "$APT_GET_SOURCE" || APT_GET_SOURCE="apt-get source"
test -n "$FAKEROOT" || FAKEROOT="fakeroot"

verb() {
  test $V -gt 0 && printf "$0: "$*
}

die() {
  printf "$0: Error: "$*
  exit 1
}

get_version() {
  dpkg-query -f='${Version} ${Revision}\n' -W $1 2> /dev/null | \
    sort -n | tail -n 1 |
    while read version_ revision_; do
      echo "pkg_version=$(echo $version_ | sed -e 's/-.*//')"
      echo "deb_version=$(echo $version_ | sed -e 's/[^-]*-\(.*\)/\1/')"
      echo "deb_revision=$revision_"
    done
}

is_installed() {
  name="$1"
  vers="$2"
  dpkg-query -f='${Status} ${Version}\n' -W $name 2> /dev/null |
    egrep -q "^install ok installed" |
    while read ii_vers; do
        test "x$ii_vers" = "x$vers"
        return $?
    done
}
build_binutils() {
  local pkg_version
  local deb_version
  local deb_revision
  eval $(get_version binutils)
  verb "Building binutils-%s" $pkg_version
  binutils_name="binutils-$pkg_version"
  test -d $binutils_name || $APT_GET_SOURCE binutils
  cd $binutils_name || die "Failed to get $binutils_name"
  $FAKEROOT debian/rules binary-cross
  cd -
}
install_binutils() {
  local pkg_version
  local deb_version
  local deb_revision
  eval $(get_version binutils)
  is_installed binutils-${TARGET} ${pkg_version} && return 0
  verb "Installing binutils-%s_%s" $TARGET $pkg_version
  ls -t1 binutils-${TARGET}_${pkg_version}*.deb | head -n 1 | xargs dpkg -i ||
    die "Failed to install binutils-${TARGET}_${pkg_version}"
}

uclibc_headers() {
  local pkg_version
  local deb_version
  local deb_revision
  eval $(get_version uclibc)
  if test "No$pkg_version" = "No"
  then
    # not available, fallback
    pkg_version="0.9.30.1"
    uclibc_name="uClibc-$pkg_version"
    test -d $uclibc_name &&
      svn up $uclibc_name ||
      svn co svn://uclibc.org/branches/uClibc_0_9_30 $uclibc_name
rm -rf $uclibc_name/debian
cp -a uClibc-0.9.30.1-debian $uclibc_name/debian
  fi
  verb "uClibc-%s" $pkg_version
  uclibc_name="uClibc-$pkg_version"
  test -d $uclibc_name || $APT_GET_SOURCE uclibc
  cd $uclibc_name || die "Failed to get $uclibc_name"
  $FAKEROOT debian/rules binary-indep
  cd -
}
install_uclibc_headers() {
  local pkg_version
  local deb_version
  local deb_revision
  eval $(get_version uclibc)
  if test "No$pkg_version" = "No"
  then
    # not available, fallback
    pkg_version="0.9.30.1"
    uclibc_name="uClibc-$pkg_version"
  fi
  is_installed uclibc-${TARGET} ${pkg_version} && return 0
  verb "Installing uclibc-%s_%s" $TARGET $pkg_version
  ls -t1 uclibc-${TARGET}_${pkg_version}*.deb | head -n 1 | xargs dpkg -i ||
    die "Failed to install uclibc-${TARGET}_${pkg_version}"
}

uclibc_runtime_and_devel() {
  local pkg_version
  local deb_version
  local deb_revision
  eval $(get_version uclibc)
  if test "No$pkg_version" = "No"
  then
    # not available, fallback
    pkg_version="0.9.30.1"
    uclibc_name="uClibc-$pkg_version"
    test -d $uclibc_name &&
      svn up $uclibc_name ||
      svn co svn://uclibc.org/branches/uClibc_0_9_30 $uclibc_name
rm -rf $uclibc_name/debian
cp -a uClibc-0.9.30.1-debian $uclibc_name/debian
  fi
  verb "uClibc-%s" $pkg_version
  uclibc_name="uClibc-$pkg_version"
  test -d $uclibc_name || $APT_GET_SOURCE uclibc
  cd $uclibc_name || die "Failed to get $uclibc_name"
  $FAKEROOT debian/rules binary-runtime
  $FAKEROOT debian/rules binary-devel
  cd -
}

build_initial_cross_gcc() {
  local pkg_name
  local pkg_version
  local deb_version
  local deb_revision
  pkg_name="gcc"
  eval $(get_version $pkg_name-[[:digit:]]*\.[[:digit:]]*)
  # XXX: revisit ${Package} later
  pkg_name="gcc-4.3"
  pkg_version="4.3.3"
  if test "No$pkg_version" = "No"
  then
    # not available, fallback
    pkg_name="gcc"
    pkg_version="4.4.0"
    gcc_name="gcc-$pkg_version"
    test -d $gcc_name &&
      svn up $gcc_name ||
      svn co svn://gcc.gnu.org/svn/gcc/trunk $gcc_name
rm -rf $gcc_name/debian
cp -a gcc-${pkg_version}-debian $gcc_name/debian
  fi
  gcc_name="$pkg_name-$pkg_version"
  verb "%s-%s" $pkg_name $pkg_version
  test -d $gcc_name || $APT_GET_SOURCE $pkg_name
  cd $gcc_name || die "Failed to get $gcc_name"
  # honour our idea of languages that we need, else fallback to old values
  sed -i -e '/^  languages = c /c \
  ifeq ($(LANGUAGES),)\
    languages = c c++ objc objpp\
  else\
    languages = $(LANGUAGES)\
  endif' debian/rules.conf
  # heck, and the same goes for addons
  sed -i -e '/^  addons = libgcc /c \
  ifeq ($(ADDONS),)\
    addons = libgcc lib64gcc lib32gcc libcxx lib32cxx lib64cxx libn32cxx cdev c++dev libobjc objcdev objppdev gccxbase\
  else\
    addons = $(ADDONS)\
  endif' debian/rules.conf

  GCC_TARGET=$TARGET DEB_CROSS__=yes LANGUAGES=c ADDONS=libgcc debian/rules control
  GCC_TARGET=$TARGET DEB_CROSS__=yes LANGUAGES=c ADDONS=libgcc dpkg-buildpackage -rfakeroot
  cd -
}


build_binutils
install_binutils
uclibc_headers
install_uclibc_headers
build_initial_cross_gcc
install_initial_cross_gcc
# uclibc_runtime_and_devel

