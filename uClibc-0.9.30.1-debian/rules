#!/usr/bin/make -f

p_bin = uclibc
p_ini = lib$(p_bin)-initial
p_dev = lib$(p_bin)-dev
p_mul = lib$(p_bin)-multiarch
p_doc = lib$(p_bin)-doc
p_src = $(p_bin)-source

pwd   := $(shell pwd)
d_bin = debian/tmp
d_ini = debian/$(p_ini)
d_dev = debian/$(p_dev)
d_mul = debian/$(p_mul)
d_doc = debian/$(p_doc)
d_src = debian/$(p_src)

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

CFLAGS = -Wall -g

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -Os
endif

DEB_BUILD_ARCH ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH)
DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
DEB_HOST_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)

UCLIBC_HOST_GNU_TYPE ?= $(TARGET)

ifneq ($(DEB_HOST_ARCH),$(DEB_BUILD_ARCH))
CROSS = $(DEB_HOST_GNU_TYPE)-
else
CROSS =
endif

ifeq ($(DEB_HOST_ARCH),mipsel)
KERNEL_ARCH = mips
else
ifeq ($(DEB_HOST_ARCH),amd64)
KERNEL_ARCH = x86_64
else
ifeq ($(DEB_HOST_ARCH),armel)
KERNEL_ARCH = arm
else
KERNEL_ARCH = $(DEB_HOST_ARCH)
endif
endif
endif

HOSTCC = gcc
CC = $(CROSS)gcc

UCLIBC_VERSION := 0.9.30
KERNEL_VERSION := $(shell sed -ne '/^Build-Depends: /s/.*linux-source-\([0-9\.]*\),*.*/\1/p' debian/control)
#KERNEL_SOURCE := $(shell pwd)/kernel/linux-source-$(KERNEL_VERSION)
KERNEL_SOURCE := $(pwd)/linux-source/linux-$(KERNEL_VERSION)
k_dev = debian/kernel-libc-dev

ifeq ($(KERNEL_VERSION),)
$(error Could not determine kernel version)
endif

DEVEL_PREFIX = /usr/$(UCLIBC_HOST_GNU_TYPE)
source_files = $(addprefix $(shell basename $(pwd))/, \
	$(filter-out %-stamp .svn linux-source debian builddir-% test-summary, $(wildcard *)))

STAMPS := unpack-kernel uclibc-source configured install_headers \
		install_runtime install_dev
STAMP = touch $@-stamp

$(patsubst %,%-stamp,$(STAMPS)): %-stamp: %

/usr/src/linux-source-$(KERNEL_VERSION).tar.bz2:
	wget -O $@ http://www.kernel.org/pub/linux/kernel/v2.6/linux-$(KERNEL_VERSION).tar.bz2

unpack-kernel: /usr/src/linux-source-$(KERNEL_VERSION).tar.bz2
	dh_testdir
	$(RM) -r $(pwd)/linux-source $(pwd)/$(k_dev)
	mkdir $(pwd)/linux-source
	cd $(pwd)/linux-source && tar -xjf /usr/src/linux-source-$(KERNEL_VERSION).tar.bz2
	$(MAKE) -C $(KERNEL_SOURCE) ARCH=$(KERNEL_ARCH) HOSTCC=$(HOSTCC) \
		defconfig
	$(MAKE) -C $(KERNEL_SOURCE) ARCH=$(KERNEL_ARCH) HOSTCC=$(HOSTCC) \
		INSTALL_HDR_PATH=$(pwd)/$(k_dev) headers_install
	$(STAMP)

uclibc-source:
	dh_testdir
	install -d $(pwd)/$(d_src)/DEBIAN $(pwd)/$(d_src)/usr/src/uClibc
	tar -cj -C .. --exclude=.svn \
		-f $(pwd)/$(d_src)/usr/src/uClibc/uclibc-$(UCLIBC_VERSION).tar.bz2 \
		$(source_files)
	dh_gencontrol -- -isp -P$(d_src) -p$(p_src)
	chown -R 0:0 $(d_src)
	chmod -R go=rX $(d_src)
	dh_md5sums
	dh_builddeb
	dh_clean
	$(STAMP)

configured: unpack-kernel-stamp uclibc-source-stamp
	dh_testdir
	cat debian/config-$(DEB_HOST_ARCH) debian/config-common | \
	  sed -e 's@\(KERNEL_HEADERS *= *\).*@\1"$(pwd)/$(k_dev)"@' \
	      -e 's@\(DEVEL_PREFIX *= *\).*@\1"$(DEVEL_PREFIX)"@' \
	      -e 's@\(CROSS_COMPILER_PREFIX *= *\).*@\1"$(CROSS)"@' \
	  > .config
	$(MAKE) HOSTCC="$(HOSTCC)" oldconfig
	$(STAMP)

install_headers: unpack-kernel-stamp configured-stamp
	dh_testdir
	dh_testroot
	$(MAKE) HOSTCC="$(HOSTCC)" oldconfig
	$(MAKE) HOSTCC="$(HOSTCC)" \
		PREFIX=$(pwd)/$(d_dev) \
		$@
	$(STAMP)

install_runtime: unpack-kernel-stamp configured-stamp
	dh_testdir
	$(MAKE) HOSTCC="$(HOSTCC)" $@
	$(STAMP)

install_dev: unpack-kernel-stamp configured-stamp
	dh_testdir
	$(MAKE) HOSTCC="$(HOSTCC)" $@
	$(STAMP)

build: unpack-kernel-stamp configured-stamp
	dh_testdir
	#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	##$(MAKE) CROSS="$(CROSS)" CC="$(CC)" headers
	##cd extra/locale && find charmaps -name "*.pairs" >codesets.txt
	##$(MAKE) -C extra/locale
	$(MAKE) CROSS="$(CROSS)" CC="$(CC)" 

clean:
	dh_testdir
	dh_testroot
	$(RM) configured-stamp unpack-kernel-stamp install_headers-stamp \
		install_dev-stamp \
		install_runtime-stamp install_hostutils-stamp
	$(RM) -r kernel
	$(RM) debian/uclibc-dev.files debian/ldconfig.1 debian/uclibc.shlibs \
	  debian/ldd.1 debian/uclibc.postinst debian/uclibc.files \
	  debian/uclibc-dev.prerm debian/uclibc.prerm debian/dirs \
	  debian/docs debian/uclibc-dev.postinst
	$(MAKE) distclean
	dh_clean 

install: build
	#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	dh_testdir
	dh_testroot
	dh_clean -k 
	$(MAKE) PREFIX=$(pwd)/debian/tmp install
	$(RM) debian/tmp/lib/libnsl* debian/tmp/usr/lib/libnsl.so
	dh_install --fail-missing


# Build architecture-independent files here. (for the stage compiler)
binary-indep: install_headers-stamp
	dh_installdeb
	dh_gencontrol -- -isp -P$(d_ini) -p$(p_ini)
	dh_md5sums
	dh_builddeb

# Build architecture-dependent runtime files here. (dynamic)
binary-runtime: install_runtime-stamp
	dh_testdir
	dh_testroot
	dh_installchangelogs Changelog
	dh_installdocs
	dh_installexamples
#	dh_install
#	dh_installmenu
#	dh_installdebconf	
#	dh_installlogrotate
#	dh_installemacsen
#	dh_installpam
#	dh_installmime
#	dh_installinit
#	dh_installcron
#	dh_installinfo
	dh_installman
	dh_link
	dh_strip
	dh_compress
	dh_fixperms -Xld-uClibc
#	dh_perl
#	dh_python
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps -Luclibc$(UCLIBC_VERSION) -Xlibc-uClibc-$(UCLIBC_VERSION).so -Xld-uClibc-$(UCLIBC_VERSION).so
	dh_gencontrol -- -isp -P$(d_bin) -p$(p_bin)
	dh_md5sums
	dh_builddeb

# Build architecture-dependent development files here (archives).
binary-devel: install_dev-stamp
	dh_testdir
	dh_testroot
	dh_installchangelogs Changelog
	dh_installdocs
	dh_installexamples
#	dh_install
#	dh_installmenu
#	dh_installdebconf	
#	dh_installlogrotate
#	dh_installemacsen
#	dh_installpam
#	dh_installmime
#	dh_installinit
#	dh_installcron
#	dh_installinfo
	dh_installman
	dh_link
	dh_strip
	dh_compress
	dh_fixperms -Xld-uClibc
#	dh_perl
#	dh_python
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps -Luclibc$(UCLIBC_VERSION) -Xlibc-uClibc-$(UCLIBC_VERSION).so -Xld-uClibc-$(UCLIBC_VERSION).so
	dh_gencontrol -- -isp -P$(d_dev) -p$(p_dev)
	dh_md5sums
	dh_builddeb

runtime: binary-runtime
devel: binary-devel
.PHONY: clean binary-indep binary-runtime binary-devel
